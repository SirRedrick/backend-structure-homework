var jwt = require('jsonwebtoken');

module.exports = type => (req, res, next) => {
  const token = req.headers['authorization']?.replace('Bearer ', '');
  if (!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (type === 'admin') {
      if (tokenPayload.type != 'admin') {
        throw new Error();
      }
    }
    req.tokenPayload = tokenPayload;
    next();
  } catch (err) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
};
