module.exports = (schema, target) => (req, res, next) => {
  const { error } = schema.validate(req[target]);
  if (error) {
    return res.status(400).send({ error: error.details[0].message });
  } else {
    next();
  }
};
