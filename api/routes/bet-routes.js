const { Router } = require('express');
const betSchemas = require('../../data/schemas/bet-schemas');
const jwtMiddleware = require('../middlewares/jwt-middleware');
const validationMiddleware = require('../middlewares/validation-middleware');
const betService = require('../services/bet-service');

const router = Router();

module.exports = router.post(
  '/',
  validationMiddleware(betSchemas.post, 'body'),
  jwtMiddleware(),
  (req, res, next) => {
    const userId = req.tokenPayload.id;

    betService
      .create(userId, req.body)
      .then(result => res.send(result))
      .catch(next);
  }
);
