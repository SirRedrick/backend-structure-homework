const { Router } = require('express');
const eventSchema = require('../../data/schemas/event-schemas');
const jwtMiddleware = require('../middlewares/jwt-middleware');
const validationMiddleware = require('../middlewares/validation-middleware');
const eventService = require('../services/event-service');

const router = Router();

module.exports = router
  .post(
    '/',
    validationMiddleware(eventSchema.post, 'body'),
    jwtMiddleware('admin'),
    (req, res, next) =>
      eventService
        .create(req.body)
        .then(result => res.send(result))
        .catch(next)
  )
  .put(
    '/:id',
    validationMiddleware(eventSchema.putById, 'body'),
    jwtMiddleware('admin'),
    (req, res, next) => {
      const id = req.params.id;
      return eventService
        .update(id, req.body)
        .then(result => res.send(result))
        .catch(next);
    }
  );
