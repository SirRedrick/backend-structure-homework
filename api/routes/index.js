const healthRoutes = require('./health-routes.js');
const userRoutes = require('./user-routes.js');
const transactionRoutes = require('./transaction-routes.js');
const eventRoutes = require('./event-routes.js');
const betRoutes = require('./bet-routes.js');

module.exports = app => {
  app.use('/health', healthRoutes);
  app.use('/users', userRoutes);
  app.use('/transactions', transactionRoutes);
  app.use('/events', eventRoutes);
  app.use('/bets', betRoutes);
};
