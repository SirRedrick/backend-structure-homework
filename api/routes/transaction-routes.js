const { Router } = require('express');
const transactionSchemas = require('../../data/schemas/transaction-schemas');
const jwtMiddleware = require('../middlewares/jwt-middleware');
const validationMiddleware = require('../middlewares/validation-middleware');
const transactionService = require('../services/transaction-service');

const router = Router();

module.exports = router.post(
  '/',
  validationMiddleware(transactionSchemas.post, 'body'),
  jwtMiddleware('admin'),
  (req, res, next) =>
    transactionService
      .create(req.body)
      .then(result => res.send(result))
      .catch(next)
);
