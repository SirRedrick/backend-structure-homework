var ee = require('events');
var statEmitter = new ee();
var jwt = require('jsonwebtoken');

const schemas = require('../../data/schemas/user-schemas');
const validationMiddleware = require('../middlewares/validation-middleware');

const { Router } = require('express');
const jwtMiddleware = require('../middlewares/jwt-middleware');
const userService = require('../services/user-service');
const { nextTick } = require('process');
const userSchemas = require('../../data/schemas/user-schemas');

const router = Router();

module.exports = router
  .get('/:id', validationMiddleware(userSchemas.getById, 'params'), (req, res, next) =>
    userService
      .getById(req.params.id)
      .then(result => res.send(result))
      .catch(next)
  )
  .post('/', validationMiddleware(userSchemas.post, 'body'), (req, res) =>
    userService
      .create(req.body)
      .then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;
        statEmitter.emit('newUser');
        return res.send({
          ...result,
          accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET)
        });
      })
      .catch(err => {
        if (err.code == '23505') {
          res.status(400).send({
            error: err.detail
          });
          return;
        }
        res.status(500).send(`Internal Server Error`);
        return;
      })
  )
  .put('/:id', validationMiddleware(userSchemas.putById, 'body'), jwtMiddleware(), (req, res) => {
    if (req.params.id !== req.tokenPayload.id) {
      return res.status(401).send({ error: 'UserId mismatch' });
    } else {
      userService
        .update(req.params.id, req.body)
        .then(([result]) => {
          return res.send({
            ...result
          });
        })
        .catch(err => {
          if (err.code == '23505') {
            console.log(err);
            res.status(400).send({
              error: err.detail
            });
            return;
          }
          res.status(500).send('Internal Server Error');
          return;
        });
    }
  });
