const { betRepository } = require('../../data/repositories');

module.exports = {
  create: (userId, body) => betRepository.create(userId, body)
};
