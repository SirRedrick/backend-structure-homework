const { eventRepository } = require('../../data/repositories');

module.exports = {
  create: body => eventRepository.create(body),
  update: (eventId, body) => eventRepository.update(eventId, body)
};
