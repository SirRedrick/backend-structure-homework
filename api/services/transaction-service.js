const { transactionRepository } = require('../../data/repositories');

module.exports = {
  create: body => transactionRepository.create(body)
};
