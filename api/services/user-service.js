const { userRepository } = require('../../data/repositories');

module.exports = {
  getById: id => userRepository.getById(id),
  create: body => userRepository.create(body),
  update: (id, body) => userRepository.update(id, body)
};
