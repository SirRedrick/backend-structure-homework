const knex = require('knex');
const dbConfig = require('../../knexfile');
db = knex(dbConfig.development);

module.exports = class BaseRepository {
  constructor() {
    this.db = db;
  }
};
