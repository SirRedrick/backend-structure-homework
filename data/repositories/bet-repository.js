const BaseRepository = require('./base-repository');

module.exports = class BetRepository extends BaseRepository {
  constructor() {
    super();
  }

  create(userId, body) {
    body.event_id = body.eventId;
    body.bet_amount = body.betAmount;
    delete body.eventId;
    delete body.betAmount;
    body.user_id = userId;
    return this.db
      .select()
      .table('user')
      .then(users => {
        console.log(users);

        const user = users.find(u => u.id == userId);
        if (!user) {
          return Promise.reject({ status: 400, message: 'User does not exist' });
        }
        if (+user?.balance < +body.bet_amount) {
          return Promise.reject({ status: 400, message: 'Not enough balance' });
        }
        return this.db('event')
          .where('id', body.event_id)
          .then(([event]) => {
            if (!event) {
              return Promise.reject({ status: 404, message: 'Event not found' });
            }
            return this.db('odds')
              .where('id', event.odds_id)
              .then(([odds]) => {
                if (!odds) {
                  return Promise.reject({ status: 404, message: 'Odds not found' });
                }
                let multiplier;
                switch (body.prediction) {
                  case 'w1':
                    multiplier = odds.home_win;
                    break;
                  case 'w2':
                    multiplier = odds.away_win;
                    break;
                  case 'x':
                    multiplier = odds.draw;
                    break;
                }
                return this.db('bet')
                  .insert({
                    ...body,
                    multiplier,
                    event_id: event.id
                  })
                  .returning('*')
                  .then(([bet]) => {
                    var currentBalance = user.balance - body.bet_amount;
                    return this.db('user')
                      .where('id', userId)
                      .update({
                        balance: currentBalance
                      })
                      .then(() => {
                        statEmitter.emit('newBet');
                        [
                          'bet_amount',
                          'event_id',
                          'away_team',
                          'home_team',
                          'odds_id',
                          'start_at',
                          'updated_at',
                          'created_at',
                          'user_id'
                        ].forEach(whatakey => {
                          var index = whatakey.indexOf('_');
                          var newKey = whatakey.replace('_', '');
                          newKey = newKey.split('');
                          newKey[index] = newKey[index].toUpperCase();
                          newKey = newKey.join('');
                          bet[newKey] = bet[whatakey];
                          delete bet[whatakey];
                        });
                        return {
                          ...bet,
                          currentBalance: currentBalance
                        };
                      });
                  });
              });
          });
      });
  }
};
