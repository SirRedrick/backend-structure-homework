const ee = require('events');
const statEmitter = new ee();
const BaseRepository = require('./base-repository');

module.exports = class EventRepository extends BaseRepository {
  constructor() {
    super();
  }

  create(body) {
    body.odds.home_win = body.odds.homeWin;
    delete body.odds.homeWin;
    body.odds.away_win = body.odds.awayWin;
    delete body.odds.awayWin;

    return this.db('odds')
      .insert(body.odds)
      .returning('*')
      .then(([odds]) => {
        delete body.odds;
        body.away_team = body.awayTeam;
        body.home_team = body.homeTeam;
        body.start_at = body.startAt;
        delete body.awayTeam;
        delete body.homeTeam;
        delete body.startAt;
        return this.db('event')
          .insert({
            ...body,
            odds_id: odds.id
          })
          .returning('*')
          .then(([event]) => {
            statEmitter.emit('newEvent');
            [
              'bet_amount',
              'event_id',
              'away_team',
              'home_team',
              'odds_id',
              'start_at',
              'updated_at',
              'created_at'
            ].forEach(whatakey => {
              var index = whatakey.indexOf('_');
              var newKey = whatakey.replace('_', '');
              newKey = newKey.split('');
              newKey[index] = newKey[index].toUpperCase();
              newKey = newKey.join('');
              event[newKey] = event[whatakey];
              delete event[whatakey];
            });
            ['home_win', 'away_win', 'created_at', 'updated_at'].forEach(whatakey => {
              var index = whatakey.indexOf('_');
              var newKey = whatakey.replace('_', '');
              newKey = newKey.split('');
              newKey[index] = newKey[index].toUpperCase();
              newKey = newKey.join('');
              odds[newKey] = odds[whatakey];
              delete odds[whatakey];
            });
            return {
              ...event,
              odds
            };
          });
      });
  }

  update(eventId, body) {
    return this.db('bet')
      .where('event_id', eventId)
      .andWhere('win', null)
      .then(bets => {
        var [w1, w2] = body.score.split(':');
        let result;
        if (+w1 > +w2) {
          result = 'w1';
        } else if (+w2 > +w1) {
          result = 'w2';
        } else {
          result = 'x';
        }
        return this.db('event')
          .where('id', eventId)
          .update({ score: body.score })
          .returning('*')
          .then(([event]) => {
            if (!event) {
              Promise.reject({ status: 404, message: 'Event not found' });
            }
            Promise.all(
              bets.map(bet => {
                if (bet.prediction == result) {
                  this.db('bet').where('id', bet.id).update({
                    win: true
                  });
                  this.db('user')
                    .where('id', bet.user_id)
                    .then(([user]) => {
                      return this.db('user')
                        .where('id', bet.user_id)
                        .update({
                          balance: user.balance + bet.bet_amount * bet.multiplier
                        });
                    });
                } else if (bet.prediction != result) {
                  return this.db('bet').where('id', bet.id).update({
                    win: false
                  });
                }
              })
            );
            [
              'bet_amount',
              'event_id',
              'away_team',
              'home_team',
              'odds_id',
              'start_at',
              'updated_at',
              'created_at'
            ].forEach(whatakey => {
              var index = whatakey.indexOf('_');
              var newKey = whatakey.replace('_', '');
              newKey = newKey.split('');
              newKey[index] = newKey[index].toUpperCase();
              newKey = newKey.join('');
              event[newKey] = event[whatakey];
              delete event[whatakey];
            });
            return event;
          });
      });
  }
};
