const TransactionRepository = require('./transaction-repository');
const UserRepository = require('./user-repository');
const EventRepository = require('./event-repository');
const BetRepository = require('./bet-repository');

const userRepository = new UserRepository();
const transactionRepository = new TransactionRepository();
const eventRepository = new EventRepository();
const betRepository = new BetRepository();

module.exports = {
  userRepository,
  transactionRepository,
  eventRepository,
  betRepository
};
