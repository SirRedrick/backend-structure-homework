const BaseRepository = require('./base-repository');

module.exports = class TransactionRepository extends BaseRepository {
  constructor() {
    super();
  }

  create(body) {
    const { userId } = body;

    return db('user')
      .where('id', userId)
      .then(([user]) => {
        if (!user) {
          return Promise.reject({ status: 400, message: 'User does not exist' });
        }

        body.card_number = body.cardNumber;
        delete body.cardNumber;
        body.user_id = body.userId;
        delete body.userId;

        return db('transaction')
          .insert(body)
          .returning('*')
          .then(([result]) => {
            const currentBalance = body.amount + user.balance;
            return db('user')
              .where('id', body.user_id)
              .update('balance', currentBalance)
              .then(() => {
                ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {
                  const index = whatakey.indexOf('_');
                  let newKey = whatakey.replace('_', '');

                  newKey = newKey.split('');
                  newKey[index] = newKey[index].toUpperCase();
                  newKey = newKey.join('');
                  result[newKey] = result[whatakey];
                  delete result[whatakey];
                });
                return {
                  ...result,
                  currentBalance
                };
              });
          });
      });
  }
};
