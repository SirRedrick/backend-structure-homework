const BaseRepository = require('./base-repository');

module.exports = class UserRepository extends BaseRepository {
  constructor() {
    super();
  }

  getById(id) {
    return this.db('user')
      .where('id', id)
      .returning('*')
      .then(([result]) => {
        if (!result) {
          return Promise.reject({ status: 404, message: 'User not found' });
        } else {
          return {
            ...result
          };
        }
      });
  }

  create(body) {
    body.balance = 0;
    return this.db('user').insert(body).returning('*');
  }

  update(id, body) {
    return this.db('user').where('id', id).update(body).returning('*');
  }
};
