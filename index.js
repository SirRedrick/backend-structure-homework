var express = require('express');
var knex = require('knex');
var jwt = require('jsonwebtoken');
var joi = require('joi');
var ee = require('events');
var statEmitter = new ee();

var dbConfig = require('./knexfile');
const router = require('./api/routes');
const errorMiddleware = require('./api/middlewares/error-middleware');
var app = express();

var port = 3000;

var stats = {
  totalUsers: 3,
  totalBets: 1,
  totalEvents: 1
};

var db;
app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db = knex(dbConfig.development);
  db.raw('select 1+1 as result')
    .then(function () {
      neededNext();
    })
    .catch(() => {
      throw new Error('No db connection');
    });
});

router(app);

app.use(errorMiddleware);

app.get('/stats', (req, res) => {
  try {
    let token = req.headers['authorization'];
    if (!token) {
      return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
      var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type != 'admin') {
        throw new Error();
      }
    } catch (err) {
      return res.status(401).send({ error: 'Not Authorized' });
    }
    res.send(stats);
  } catch (err) {
    console.log(err);
    res.status(500).send('Internal Server Error');
    return;
  }
});

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
